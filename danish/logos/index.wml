#use wml::debian::template title="Debians logoer"
#use wml::debian::translation-check translation="c05e77c2a846461c3f24acec26b086fc9e20d143"
#include "$(ENGLISHDIR)/logos/index.data"

<p>Debian har to logoer (bomærker).  Det <a href="#open-use">officielle logo</a> 
(også kendt som <q>open use logo</q>, eller på dansk <q>åben 
anvendelse-logoet</q>) indeholder Debians velkendte <q>hvirvel</q> og 
repræsenterer bedst Debian-projektets visuelle identitet.  Et andet 
<a href="#restricted-use">begrænset anvendelse-logo</a> findes også, kun til 
brug af Debian-projektet og dets medlemmer.  For at referere til Debian bedes 
man benytte åben anvendelse-logoet.</p>

<hr>

<table cellspacing="0" cellpadding="5" width="100%">
    <colgroup span="2">
    <col width="65%">
    <col width="35%">
    <tr>
	<th colspan="2"><a name="open-use">Debian Open Use Logo</a></th>
    </tr>
    <tr>
	<td>
    	    <p>Debian Open Use Logo (Debians åben anvendelse-logo) findes i 
    	    to varianter, et med og et uden teksten &ldquo;Debian&rdquo;.</p>

    	    <p>Debian Open Use Logo(s) er ophavsretligt beskyttet (c) 1999 af
    	    <a href="https://www.spi-inc.org/">Software in the Public Interest, 
    	    Inc.</a>, og frigivet under betingelserne i 
    	    <a href="https://www.gnu.org/copyleft/lgpl.html">GNU Lesser General 
    	    Public License</a>, version 3 eller enhver senere version, eller, 
    	    valgfrit, under 
    	    <a href="http://creativecommons.org/licenses/by-sa/3.0/deed.da">\
    	    Creative Commons Attribution-ShareAlike 3.0 Unported License</a> 
    	    (Creative Commons Navngivelse-DelPåSammeVilkår 3.0 Unported).</p>

    	    <p>Bemærk: vi ser meget gerne at du lader billedet pege på 
    	    <a href="$(HOME)">https://www.debian.org/</a>, hvis du bruger det på 
    	    en webside.</p>
	</td>
	<td>
    	    <openlogotable>
	</td>
    </tr>
</table>

<hr>

<table cellspacing="0" cellpadding="5" width="100%">
    <colgroup span="2">
    <col width="65%">
    <col width="35%">
    <tr>
	<th colspan="2"><a name="restricted-use">Debian Restricted Use Logo License</a></th>
    </tr>
    <tr>
	<td>
    	    <h3>Debian Restricted Use Logo-licens</h3>

    	    <p>Ophavsretsligt beskyttet (c) 1999 Software in the Public 
    	    Interest</p>

    	    <p>Bemærk! Den <a href="index.en.html">engelske side</a> indeholder den
    	    oprindelige ordlyd, dette er blot en oversættelse.</p>

    	    <ol>
    		<li>Dette logo må kun anvendes hvis:
        	    <ul>
  			<li>produktet det benyttes i forbindelse med anvender en 
  			    dokumenteret procedure som er offentliggjort på 
  			    www.debian.org (for eksempel fremstilling af 
  			    officielle cd'er), eller hvis</li>
        		<li>Debian officielt har godkendt formålet med 
        		    anvendelsen</li>
        	    </ul>
    		</li>
    		<li>Det må benyttes hvis en officiel del af Debian (afgøres ved 
    		    hjælp af reglerne i I) er en del af det komplette produkt, 
    		    hvis det gøres klart at kun denne del er officielt 
    		    godkendt</li>
    		<li>Vi forbeholder os ret til at ophæve licensen til et 
    		    produkt</li>
    	    </ol>

    	    <p>Det er tilladt at benytte begrænset anvendelse-logo på tøj 
    	    (trøjer, hatte, osv.) så længe de fremstilles af en Debian-udvikler 
    	    og ikke sælges for at tjene penge.</p>
	</td>
	<td>
    	    <officiallogotable>
	</td>
    </tr>
</table>

<hr>


<h2>Om Debians logoer</h2>

<p>Der blev stemt om Debians logoer (bomærker) blandt Debian-udviklerne i 1999.  
De er lavet af <a href="mailto:rsilva@debian.org">Raul Silva</a>.  Den anvendte 
røde farve i skrifttypen er nominelt <strong>Rubine Red 2X CVC</strong>.  Den 
Opdateret svarer det enten til PANTONE Strong Red C (renderet i RGB som #CE0056) 
eller PANTONE Rubine Red C (renderet i RGB som #CE0058).  Der er flere 
oplysninger om logoerne og årsagen til at PANTONE Rubine Red 2X CVC er blevet 
erstattet, samt om andre tilsvarende røde farver på 
<a href="https://wiki.debian.org/DebianLogo">Debian-logoets wikiside</a>.</p>


<h2>Andre markedsføringsbilleder</h2>

<h3>Debian-knapper</h3>

<p><img class="ico" src="button-1.gif" alt="[Debian GNU/Linux]">
Denne var den første knap, som blev lavet til projektet.  Licensen til logoet 
svarer til Open Use-logoets licens.  Knappen er lavet af 
<a href="mailto:csmall@debian.org">Craig Small</a>.</p>

<p>Her er nogle flere knapper, som er lavet til Debian:
<br />
<morebuttons>


<h3>Debian Diversity-logo</h3>

<p>Der er flere varianter af Debians logo, med det formål at fremme 
mangfoldighed i vores fællesskab; det kaldes Debian Diversity-logoet:
<br/>
<img src="diversity-2019.png" alt="[Debian Diversity-logo]" />
<br/>
Logoet blev lavet af <a href="https://wiki.debian.org/ValessioBrito">Valessio 
Brito</a>, og er lisenceret under GPLv3.  Kilden (SVG-format) er tilgængelig
i skaberens <a href="https://gitlab.com/valessiobrito/artwork">Git-arkiv</a>.
<br />
</p>


<h3>Sekskantet Debian-klistermærke</h3>

<p>Her er et klistermærke, som følger 
<a href="https://github.com/terinjokes/StickerConstructorSpec">speficikationen 
for hexagonale klistermærker</a>:
<br/>
<img src="hexagonal.png" alt="[Debian GNU/Linux]" />
<br/>
Kildekoden (i SVG-format) og en Makefile til generering af png- og 
svg-forhåndsvisningerne, er tilgænngelige fra 
<a href="https://salsa.debian.org/debian/debian-flyers/tree/master/hexagonal-sticker">\
Debian flyers-repo'et</a>.

<br />


<h3>Andre billeder</h3>

<p>Du kan finde flere pæne billeder på steder som:</p>

<table summary="Andre websteder med Debian-kunst">
    <tr>
	<td valign="bottom" align="center">
    	    <a href="http://gnuart.onshore.com/">\
    	    <img src="gnuart.png" alt="GNU/art">\
    	    </a>
    	    <br />
    	    <a href="http://gnuart.onshore.com/">GNU/art</a>
	</td>
    </tr>
</table>

<p><a href="tshirt-lt99.jpg">T-shirt-kunst</a> fra LinuxTag 99.</p>

<p>Hvis du vil hjælpe til med at reklamere for Debian på nettet, kan du 
bruge vore <a href="../banners/">bannere</a>.</p>

#
# Logo font: Poppl Laudatio Condensed
#
# https://lists.debian.org/debian-www/2003/08/msg00261.html
#
