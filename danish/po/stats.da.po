msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Statistik over oversættelser på Debians websted"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Der er %d sider at oversætte."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Der er %d bytes at oversætte."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Der er %d strenge at oversætte."

#: ../../stattrans.pl:278 ../../stattrans.pl:494
msgid "Wrong translation version"
msgstr "Forkert oversættelsesversion"

#: ../../stattrans.pl:280
msgid "This translation is too out of date"
msgstr "Denne oversættelse er forældet"

#: ../../stattrans.pl:282
msgid "The original is newer than this translation"
msgstr "Originalen er nyere end denne oversættelse"

#: ../../stattrans.pl:286 ../../stattrans.pl:494
msgid "The original no longer exists"
msgstr "Originalen findes ikke længere"

#: ../../stattrans.pl:470
msgid "hit count N/A"
msgstr "antal forekomster ikke tilgængelig"

#: ../../stattrans.pl:470
msgid "hits"
msgstr "forekomster"

#: ../../stattrans.pl:488 ../../stattrans.pl:489
msgid "Click to fetch diffstat data"
msgstr "Klik for at hente diffstat-data"

#: ../../stattrans.pl:599 ../../stattrans.pl:739
msgid "Created with <transstatslink>"
msgstr "Oprettet med <transstatslink>"

#: ../../stattrans.pl:604
msgid "Translation summary for"
msgstr "Oversættelsesresume for"

#: ../../stattrans.pl:607 ../../stattrans.pl:763 ../../stattrans.pl:809
#: ../../stattrans.pl:852
msgid "Not translated"
msgstr "Ikke oversat"

#: ../../stattrans.pl:607 ../../stattrans.pl:762 ../../stattrans.pl:808
msgid "Outdated"
msgstr "Forældet"

#: ../../stattrans.pl:607
msgid "Translated"
msgstr "Oversat"

#: ../../stattrans.pl:607 ../../stattrans.pl:687 ../../stattrans.pl:761
#: ../../stattrans.pl:807 ../../stattrans.pl:850
msgid "Up to date"
msgstr "Ajourført"

#: ../../stattrans.pl:608 ../../stattrans.pl:609 ../../stattrans.pl:610
#: ../../stattrans.pl:611
msgid "files"
msgstr "filer"

#: ../../stattrans.pl:614 ../../stattrans.pl:615 ../../stattrans.pl:616
#: ../../stattrans.pl:617
msgid "bytes"
msgstr "bytes"

#: ../../stattrans.pl:624
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Note: Listerne over sider er sorteret efter popularitet.  Svæv over "
"sidenavnet for at se antallet af forekomster."

#: ../../stattrans.pl:630
msgid "Outdated translations"
msgstr "Forældede oversættelser"

#: ../../stattrans.pl:632 ../../stattrans.pl:686
msgid "File"
msgstr "Fil"

#: ../../stattrans.pl:634
msgid "Diff"
msgstr "Diff"

#: ../../stattrans.pl:636
msgid "Comment"
msgstr "Kommentar"

#: ../../stattrans.pl:637
msgid "Diffstat"
msgstr "Diffstat"

#: ../../stattrans.pl:638
msgid "Git command line"
msgstr "Git-kommandolinje"

#: ../../stattrans.pl:640
msgid "Log"
msgstr "Log"

#: ../../stattrans.pl:641
msgid "Translation"
msgstr "Oversættelse"

#: ../../stattrans.pl:642
msgid "Maintainer"
msgstr "Vedligeholder"

#: ../../stattrans.pl:644
msgid "Status"
msgstr "Status"

#: ../../stattrans.pl:645
msgid "Translator"
msgstr "Oversætter"

#: ../../stattrans.pl:646
msgid "Date"
msgstr "Dato"

#: ../../stattrans.pl:653
msgid "General pages not translated"
msgstr "Generelle sider ikke oversat"

#: ../../stattrans.pl:654
msgid "Untranslated general pages"
msgstr "Uoversatte generelle sider"

#: ../../stattrans.pl:659
msgid "News items not translated"
msgstr "Nyheder ikke oversat"

#: ../../stattrans.pl:660
msgid "Untranslated news items"
msgstr "Uoversatte nyheder"

#: ../../stattrans.pl:665
msgid "Consultant/user pages not translated"
msgstr "Konsulent-/brugersider ikke oversat"

#: ../../stattrans.pl:666
msgid "Untranslated consultant/user pages"
msgstr "Uoversatte konsulent-/brugersider"

#: ../../stattrans.pl:671
msgid "International pages not translated"
msgstr "Internationale sider ikke oversat"

#: ../../stattrans.pl:672
msgid "Untranslated international pages"
msgstr "Uoversatte internationale sider"

#: ../../stattrans.pl:677
msgid "Translated pages (up-to-date)"
msgstr "Oversatte sider (ajourførte)"

#: ../../stattrans.pl:684 ../../stattrans.pl:834
msgid "Translated templates (PO files)"
msgstr "Oversatte skabeloner (PO-filer)"

#: ../../stattrans.pl:685 ../../stattrans.pl:837
msgid "PO Translation Statistics"
msgstr "Statistik over PO-oversættelser"

#: ../../stattrans.pl:688 ../../stattrans.pl:851
msgid "Fuzzy"
msgstr "Fuzzy"

#: ../../stattrans.pl:689
msgid "Untranslated"
msgstr "Uoversat"

#: ../../stattrans.pl:690
msgid "Total"
msgstr "Total"

#: ../../stattrans.pl:707
msgid "Total:"
msgstr "Total:"

#: ../../stattrans.pl:741
msgid "Translated web pages"
msgstr "Oversatte websider"

#: ../../stattrans.pl:744
msgid "Translation Statistics by Page Count"
msgstr "Oversættelsesstatistik efter antal sider"

#: ../../stattrans.pl:759 ../../stattrans.pl:805 ../../stattrans.pl:849
msgid "Language"
msgstr "Sprog"

#: ../../stattrans.pl:760 ../../stattrans.pl:806
msgid "Translations"
msgstr "Oversættelser"

#: ../../stattrans.pl:787
msgid "Translated web pages (by size)"
msgstr "Oversatte websider (efter størrelse)"

#: ../../stattrans.pl:790
msgid "Translation Statistics by Page Size"
msgstr "Oversættelsesstatistik efter sidestørrelse"

#~ msgid "Origin"
#~ msgstr "Ophav"

#~ msgid "Hit data from %s, gathered %s."
#~ msgstr "Forekomstdata fra %s, indsamlet %s."

#~ msgid "Created with"
#~ msgstr "Oprettet med"

#~ msgid "Commit diff"
#~ msgstr "Commitdiff"

#~ msgid "Colored diff"
#~ msgstr "Farvelagt diff"

#~ msgid "Unified diff"
#~ msgstr "Samlet diff"
