<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Tobias Stoeckmann from the OpenBSD project discovered the following
vulnerability in libX11, the X11 client-side library:</p>

    <p>Insufficient validation of data from the X server
    can cause out of boundary memory read (XGetImage())
    or write (XListFonts()).</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2:1.5.0-1+deb7u3.</p>

<p>We recommend that you upgrade your libx11 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-684.data"
# $Id: $
