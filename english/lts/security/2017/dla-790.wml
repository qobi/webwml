<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that mapserver, a CGI-based framework for Internet
map services, was vulnerable to a stack-based overflow. This issue
allowed a remote user to crash the service, or potentially execute
arbitrary code.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
6.0.1-3.2+deb7u4.</p>

<p>We recommend that you upgrade your mapserver packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-790.data"
# $Id: $
