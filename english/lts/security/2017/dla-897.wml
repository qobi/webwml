<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6503">CVE-2017-6503</a>

      <p>WebUI in qBittorrent before 3.3.11 did not escape many values,
      which could potentially lead to XSS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6504">CVE-2017-6504</a>

      <p>WebUI in qBittorrent before 3.3.11 did not set the X-Frame-Options
      header, which could potentially lead to clickjacking.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.9.8-1+deb7u1.</p>

<p>We recommend that you upgrade your qbittorrent packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-897.data"
# $Id: $
