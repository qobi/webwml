<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were a large number of NULL pointer
dereferences due to unchecked return values from <tt>malloc</tt>
and friends in hiredis, a minimalistic C client library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7105">CVE-2020-7105</a>

    <p>async.c and dict.c in libhiredis.a in hiredis through 0.14.0 allow a
    NULL pointer dereference because malloc return values are
    unchecked.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.11.0-4+deb8u1.</p>

<p>We recommend that you upgrade your hiredis packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2083.data"
# $Id: $
