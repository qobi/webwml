<define-tag pagetitle>Rilasciata Debian 10 <q>buster</q></define-tag>
<define-tag release_date>2019-07-06</define-tag>
#use wml::debian::news

##
## Translators should uncomment the following line and add their name
## Leaving translation at 1.1 is okay; that's the first version which will
## be added to Debian's webwml repository
##
#use wml::debian::translation-check translation="cbfcd50b343ab08499a29ae770a5528eda687986" maintainer="Giuseppe Sacco"


<p>Dopo 25 mesi di sviluppi, il progetto Debian &egrave; orgoglioso di presentare
la nuova versione stabile 10 (nome in codice <q>buster</q>), che sar&agrave;
supportata per i prossimi 5 anni grazie al lavoro combinato del
<a href="https://security-team.debian.org/">gruppo di sicurezza Debian</a>
e del gruppo <a href="https://wiki.debian.org/LTS">Debian Long Term
Support</a> (supporto prolungato Debian).
</p>

<p>
Debian 10 <q>buster</q> include varie applicazioni e ambienti desktop.
Tra gli altri, adesso sono compresi questi ambienti desktop:
</p>
<ul>
<li>Cinnamon 3.8,</li>
<li>GNOME 3.30,</li>
<li>KDE Plasma 5.14,</li>
<li>LXDE 0.99.2,</li>
<li>LXQt 0.14,</li>
<li>MATE 1.20,</li>
<li>Xfce 4.12.</li>
</ul>

<p>
In questo rilascio, GNOME usa il <q>display server</q> Wayland al posto di Xorg,
in maniera predefinita. Wayland ha un design più semplice e moderno, che
presenta vantaggi dal punto di vista della sicurezza. Ciononostante, Xorg è
comunque installato automaticamente e il <q>display manager</q> predefinito
permette di sceglierlo per la sessione successiva.
</p>

<p>
Grazie al progetto Reproducible Builds, oltre il 91% dei pacchetti
sorgente inclusi in Debian 10 creerà dei pacchetti binari identici
bit per bit.
Questa è una caratteristica importante di controllo che protegge gli utilizzatori
da tentativi malevoli di manomissione dei compilatori e della rete di
costruzione dei pacchetti. Le prossime versioni di Debian includeranno
degli strumenti e i metadati che permetteranno agli utilizzatori di
verificare la provenienza dei pacchetti contenuti nell'archivio.
</p>

<p>
Per coloro che operano in ambienti sensibili alla sicurezza, AppArmor, un framework
per il controllo degli accessi e la limitazione delle capacità dei programmi, è
installato e abilitato automaticamente. Inoltre, tutti i metodi forniti
da APT (eccetto cdrom, gpgv e rsh) possono opzionalmente usare il sandbox <q>seccomp-BPF</q>.
Il metodo https di APT è incluso nel pacchetto apt e non va installato a parte.
</p>

<p>
Su Debian 10 <q>buster</q> il filtraggio della rete si basa, in maniera
predefinita, sul framework nftables. A iniziare da iptables v1.8.2 il
pacchetto binario include iptables-nft e iptables-legacy, che sono due
varianti dell'interfaccia a linea di comando di iptables. La variante
basata su nftables usa il sottosistema del kernel nf_tables. Il sistema delle
<q>alternative</q> può essere utilizzato per scegliere la variante da usare.
</p>

<p>
Il supporto UEFI (<q>Unified Extensible Firmware Interface</q>) introdotto
in Debian 7 (nome in codice <q>wheezy</q>) continua ad essere migliorato
in Debian 10 <q>buster</q>. Il supporto per Secure Boot è incluso in questa
versione per le architetture amd64, i386 e arm64 e dovrebbe funzionare così
com'è su molte macchine con Secure Boot abilitato. Questo significa che gli
utilizzatori non dovrebbero più essere obbligati a disabilitare Secure Boot
nella configurazione del firmware.
</p>

<p>
In Debian 10 <q>buster</q> sono installati automaticamente i pacchetti
cups e cups-filters, fornendo agli utilizzatori tutto il necessario per
avvantaggiarsi della stampa senza driver. Le code di stampa di rete e
le stampanti IPP saranno automaticamente configurate e gestite da cups-browsed
liberandosi della necessità di installare plug-in e driver non liberi.
</p>

<p>
Debian 10 <q>buster</q> include numerosi pacchetti software aggiornati (oltre
il 62% di tutti i pacchetti della precedente versione), come:
</p>
<ul>
<li>Apache 2.4.38</li>
<li>BIND DNS Server 9.11</li>
<li>Chromium 73.0</li>
<li>Emacs 26.1</li>
<li>Firefox 60.7 (in the firefox-esr package)</li>
<li>GIMP 2.10.8</li>
<li>GNU Compiler Collection 7.4 and 8.3</li>
<li>GnuPG 2.2</li>
<li>Golang 1.11</li>
<li>Inkscape 0.92.4</li>
<li>LibreOffice 6.1</li>
<li>Linux 4.19 series</li>
<li>MariaDB 10.3</li>
<li>OpenJDK 11</li>
<li>Perl 5.28</li>
<li>PHP 7.3</li>
<li>PostgreSQL 11</li>
<li>Python 3 3.7.2</li>
<li>Ruby 2.5.1</li>
<li>Rustc 1.34</li>
<li>Samba 4.9</li>
<li>systemd 241</li>
<li>Thunderbird 60.7.2</li>
<li>Vim 8.1</li>
<li>più di 59000 altri pacchetti software pronti all'uso, costruiti
da quasi 29000 pacchetti sorgente.</li>
</ul>

<p>
Con questa ampia selezione di pacchetti e il tradizionale supporto
ad una grande serie di architetture, Debian conferma nuovamente di
essere il sistema operativo universale. È adatta a molti e diversi
scopi: dai sistemi desktop ai netbook; dai server per lo sviluppo
ai sistemi cluster; e per server di basi dati, web o persistenza (<q>storage</q>).
Allo stesso tempo, sono stati aggiunti dei controlli di qualità, come
l'installazione e aggiornamento automatico per tutti i pacchetti
nell'archivio, che permettono di eguagliare le più alte aspettative
che gli utilizzatori di <q>buster</q> si aspettano da un rilascio stabile.
</p>

<p>
In tutto sono supportate dieci architetture:
PC a 64-bit / Intel EM64T / x86-64 (<code>amd64</code>),
PC a 32-bit / Intel IA-32 (<code>i386</code>),
64-bit little-endian Motorola/IBM PowerPC (<code>ppc64el</code>),
64-bit IBM S/390 (<code>s390x</code>),
per ARM, <code>armel</code>
e <code>armhf</code> per vecchi o recenti hardware a 32-bit,
più <code>arm64</code> per le architetture a 64-bit <q>AArch64</q>,
e per MIPS, <code>mips</code> (big-endian) 
e <code>mipsel</code> (little-endian) per gli hardware a 32-bit
e <code>mips64el</code> per le architetture hardware a 64-bit little-endian.
</p>

<h3>Vuoi fare una prova?</h3>
<p>
Se vuoi semplicemente provare Debian 10 <q>buster</q> senza installarlo,
puoi usare una delle <a href="$(HOME)/CD/live/">immagini live</a> disponibili
che caricano ed eseguono il sistema operativo completo senza scrivere sui
dischi del computer.
</p>

<p>
Queste immagini live sono fornite per le architetture <code>amd64</code> e
<code>i386</code> e sono disponibili per DVD, chiavi USB, e per l'avvio via
rete. Gli utilizzatori possono scegliere tra vari ambienti desktop
da provare: Cinnamon, GNOME, KDE Plasma, LXDE, MATE, Xfce e, nuovo
in buster, LXQt. Debian Live Buster reintroduce anche l'immagine live
standard che permette di provare un sistema base Debian senza nessuna
interfaccia utente grafica.
</p>
<p>
Nel caso il sistema operativo piaccia, c'è la possibilità di procedere
con l'installazione sul disco fisso direttamente dall'immagine live.
L'immagine include l'installatore indipendente Calamares oltre a quello
standard Debian. Maggiori informazioni sono presenti nelle
<a href="$(HOME)/releases/buster/releasenotes">note di rilascio</a> e
nelle sezioni delle <a href="$(HOME)/CD/live/">immagini live per l'installazione</a>
all'interno del sito web Debian.
</p>

<p>
Per installare direttamente Debian 10 <q>buster</q> sul disco fisso del computer
si può scegliere tra diversi media di installazione, come dischi Blu-ray, DVD, CD,
chiavi USB o tramite connessioni di rete.
Alcuni ambienti desktop &mdash; Cinnamon, GNOME, KDE Plasma Desktop e applicazioni
, LXDE, LXQt, MATE e Xfce &mdash; possono essere installati direttamente dalle
immagini.
Inoltre sono disponibili CD <q>multi-architecture</q> che permettono l'installazione
per varie architetture da un singolo disco. È comunque sempre possibile
creare un media USB di installazione avviabile (vedere la
<a href="$(HOME)/releases/buster/installmanual">guida all'installazione</a>
per maggiori dettagli).
</p>

<p>
Per gli utilizzatori del cloud, Debian offre supporto diretto
per molte delle piattaforme cloud più diffuse. Si possono
selezionare immagini Debian ufficiali nei vari marketplace.
Debian pubblica anche <a
href="https://cloud.debian.org/images/openstack/current/">immagini OpenStack pre costruite</a>
per le architetture <code>amd64</code> e <code>arm64</code>, pronte
per essere scaricate e installate in configurazioni cloud locali.
</p>

<p>
Debian può essere installata adesso in 76 lingue, per la maggior parte
delle quali è possibile usare sia l'interfaccia grafica che quella
testuale.
</p>

<p>
Le immagini per l'installazione possono già essere scaricate da
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (il metodo consigliato),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a> o
<a href="$(HOME)/CD/http-ftp/">HTTP</a>; vedere
<a href="$(HOME)/CD/">Debian su CD</a> per maggiori informazioni. <q>Buster</q> sar&agrave;
anche presto disponibile su DVD, CD-ROM e dischi Blu-ray fisici presso i numerosi
<a href="$(HOME)/CD/vendors">rivenditori</a>.
</p>


<h3>Aggiornare Debian</h3>
<p>
L'aggiornamento a Debian 10 dalla precedente versione, Debian 9
(nome in codice <q>stretch</q>) è gestito automaticamente dal
sistema di gestione dei pacchetti apt per la maggior parte
delle configurazioni.
Come sempre, i sistemi Debian possono essere aggiornati senza difficoltà,
sul posto, senza nessuna indisponibilità forzata del sistema; ma è
fortemente consigliato di leggere le
<a href="$(HOME)/releases/buster/releasenotes">note di rilascio</a> e
il <a href="$(HOME)/releases/buster/installmanual">manuale d'installazione</a>
per possibili problemi e per i dettagli sull'installazione e sull'aggiornamento.
Le note di rilascio saranno ulteriomente migliorate e tradotte in
altre lingue nelle settimane successive al rilascio.
</p>


<h2>Su Debian</h2>

<p>
Debian è un sistema operativo libero, sviluppato da migliaia di
volontari di tutto il mondo, che collaborano via Internet.
I punti di forza del progetto Debian sono la sua base di volontari,
la sua dedizione al contratto sociale Debian e al software libero, e
il suo impegno a fornire il miglior sistema operativo possibile.
Questo nuovo rilascio è un passo importante in questa direzione.
</p>


<h2>Contatti</h2>

<p>
Per maggiori informazioni, visitare le pagine web Debian
su <a href="$(HOME)/">https://www.debian.org/</a> o inviare un
email a &lt;press@debian.org&gt;.
</p>
