#use wml::debian::translation-check translation="21882a152ab072f844cf526dc172ff70559e05e7"
<define-tag pagetitle>Debian 10 actualizado: publicada la versión 10.1</define-tag>
<define-tag release_date>2019-09-07</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.1</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la primera actualización de su
distribución «estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad,
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación de Debian,
los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica Debian
actualizada.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación existente a esta nueva versión haciendo
que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Corrección de fallos varios</h2>

<p>Esta actualización de la distribución «estable» añade unas pocas correcciones importantes a los paquetes siguientes:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction acme-tiny "Gestiona el próximo cambio en el protocolo ACME">
<correction android-sdk-meta "Nueva versión del proyecto original; corrige expresión regular para añadir la versión de Debian a paquetes binarios">
<correction apt-setup "Corrige la preselección de respuestas («preseeding») en Secure Apt para repositorios locales vía apt-setup/localX/">
<correction asterisk "Corrige desbordamiento de memoria en res_pjsip_messaging [AST-2019-002 / CVE-2019-12827]; corrige vulnerabilidad de caída remota en chan_sip [AST-2019-003 / CVE-2019-13161]">
<correction babeltrace "Eleva la dependencia de símbolos ctf a la versión posterior a la fusión («merge»)">
<correction backup-manager "Corrige la eliminación de archivos remotos a través de FTP o SSH">
<correction base-files "Actualizado para esta versión">
<correction basez "Decodifica correctamente cadenas codificadas en base64url">
<correction bro "Correcciones de seguridad [CVE-2018-16807 CVE-2018-17019]">
<correction bzip2 "Corrige regresión al descomprimir algunos ficheros">
<correction cacti "Corrige algunos problemas al actualizar desde la versión distribuida con stretch">
<correction calamares-settings-debian "Corrige permisos para la imagen de initramfs cuando está habilitado el cifrado de disco completo [CVE-2019-13179]">
<correction ceph "Recompilado contra la nueva libbabeltrace">
<correction clamav "Impide la extracción de bombas zip no recursivas; nueva versión «estable» del proyecto original con correcciones de seguridad - añade límite de tiempo de escaneo para mitigar los efectos de bombas zip [CVE-2019-12625]; corrige escritura fuera de límites en la biblioteca bzip2 de NSIS [CVE-2019-12900]">
<correction cloudkitty "Corrige fallos de compilación con el SQLAlchemy actualizado">
<correction console-setup "Corrige problemas de traducción a distintos idiomas al cambiar la configuración regional con Perl &gt;= 5.28">
<correction cryptsetup "Corrige soporte de cabeceras LUKS2 sin ranura de clave («keyslot») asociada; corrige desbordamiento de segmentos mapeados en arquitecturas de 32 bits">
<correction cups "Corrige múltiples problemas de seguridad y de revelación de información: problemas de desbordamientos de memoria SNMP [CVE-2019-8696 CVE-2019-8675], de desbordamiento de memoria IPP, de denegación de servicio y de revelación de contenido de la memoria en el planificador">
<correction dbconfig-common "Corrige problema provocado por un cambio en el comportamiento POSIX de bash">
<correction debian-edu-config "Usa la opción de PXE <q>ipappend 2</q> para el arranque de clientes LTSP; corrige configuración de sudo-ldap; corrige pérdida de dirección IP v4 asignada dinámicamente; varias correcciones y mejoras de debian-edu-config.fetch-ldap-cert">
<correction debian-edu-doc "Actualiza manuales y traducciones de Debian Edu Buster y de ITIL">
<correction dehydrated "Corrige la obtención de información de cuenta; continúan las correcciones de la gestión del ID de cuenta y de compatibilidad con APIv1">
<correction devscripts "debchange: objetivo buster-backports con opción --bpo">
<correction dma "No limita las conexiones TLS al uso de TLS 1.0">
<correction dpdk "Nueva versión «estable» del proyecto original">
<correction dput-ng "Añade los nombres en clave buster-backports y stretch-backports-sloppy">
<correction e2fsprogs "Corrige caídas de e4defrag en arquitecturas de 32 bits">
<correction enigmail "Nueva versión del proyecto original; correcciones de seguridad [CVE-2019-12269]">
<correction epiphany-browser "Se asegura de que la extensión web utiliza la copia de libdazzle incluida">
<correction erlang-p1-pkix "Corrige la gestión de certificados GnuTLS">
<correction facter "Corrige el análisis sintáctico de indicadores de enrutamiento de Linux que no son de la forma clave/valor (por ejemplo, onlink)">
<correction fdroidserver "Nueva versión del proyecto original">
<correction fig2dev "No provoca fallo de segmentación en puntas de flecha circulares o semicirculares con una ampliación superior a 42 [CVE-2019-14275]">
<correction firmware-nonfree "atheros: añade firmware versión WLAN.TF.2.1-00021-QCARMSWP-1 para Qualcomm Atheros QCA9377 rev 1.0; realtek: añade firmware para Realtek RTL8822CU Bluetooth; atheros: revierte cambio de firmware para QCA9377 rev 1.0 en 20180518-1; misc-nonfree: añade firmware para chips inalámbricos MediaTek MT76x0/MT76x2u, chips bluetooth MediaTek MT7622/MT7668 y firmware GV100 firmado">
<correction freeorion "Corrige caída al cargar o grabar datos de juegos">
<correction fuse-emulator "Prefiere el backend X11 al Wayland; muestra el icono de Fuse en la ventana GTK y en el diálogo «Acerca de» («About»)">
<correction fusiondirectory "Comprobaciones más estrictas en búsquedas LDAP; añade dependencia con php-xml, que faltaba">
<correction gcab "Corrige corrupción al extraer">
<correction gdb "Recompilado contra la nueva libbabeltrace">
<correction glib2.0 "Hace que el backend de configuración de GKeyFile cree ~/.config y los ficheros de configuración con permisos restrictivos [CVE-2019-13012]">
<correction gnome-bluetooth "Evita caídas de GNOME Shell cuando se utiliza gnome-shell-extension-bluetooth-quick-connect">
<correction gnome-control-center "Corrige caída al seleccionar el panel Detalles -&gt; Overview (info-overview); corrige fugas de información de memoria en el panel Acceso universal («Universal Access»); corrige una regresión que provocaba que las opciones de seguimiento del ratón de Acceso universal -&gt; Zoom no tuvieran efecto; actualiza traducciones al islandés y al japonés">
<correction gnupg2 "Adapta muchas correcciones de fallos y parches de estabilidad procedentes del proyecto original; usa keys.openpgp.org como el servidor de claves por omisión; por omisión, solo importa «self-signatures»">
<correction gnuplot "Corrige inicialización incompleta/insegura de la matriz ARGV">
<correction gosa "Comprobaciones más estrictas en búsquedas LDAP">
<correction hfst "Asegura actualizaciones desde stretch menos problemáticas">
<correction initramfs-tools "Inhabilita la reanudación («resume») cuando no hay dispositivos de intercambio («swap») adecuados; MODULES=most: incluye todos los módulos controladores de teclado, controladores cros_ec_spi y SPI, y extcon-usbc-cros-ec; MODULES=dep: incluye controladores extcon">
<correction jython "Mantiene compatibilidad hacia atrás con Java 7">
<correction lacme "Actualizado para eliminar el soporte de GET no autenticado de la API ACMEv2 de Let's Encrypt">
<correction libblockdev "Usa la API existente de cryptsetup para cambiar la palabra de paso de la ranura de clave («keyslot»)">
<correction libdatetime-timezone-perl "Actualiza los datos incluidos">
<correction libjavascript-beautifier-perl "Añade soporte del operador <q>=&gt;</q>">
<correction libsdl2-image "Corrige desbordamientos de memoria [CVE-2019-5058 CVE-2019-5052 CVE-2019-7635]; corrige acceso fuera de límites en la gestión de PCX [CVE-2019-12216 CVE-2019-12217 CVE-2019-12218 CVE-2019-12219 CVE-2019-12220 CVE-2019-12221 CVE-2019-12222 CVE-2019-5051]">
<correction libtk-img "Deja de utilizar copias internas de los codecs JPEG, Zlib y PixarLog, con lo que corrige caídas">
<correction libxslt "Corrige elusión de la infraestructura de soporte de seguridad [CVE-2019-11068], lectura del token xsl:number no inicializado [CVE-2019-13117] y lectura no inicializada con caracteres de agrupación («grouping chars») UTF-8 [CVE-2019-13118]">
<correction linux "Nueva versión «estable» del proyecto original">
<correction linux-latest "Actualizado para la ABI del núcleo 4.19.0-6">
<correction linux-signed-amd64 "Nueva versión «estable» del proyecto original">
<correction linux-signed-arm64 "Nueva versión «estable» del proyecto original">
<correction linux-signed-i386 "Nueva versión «estable» del proyecto original">
<correction lttv "Recompilado contra la nueva libbabeltrace">
<correction mapproxy "Corrige las capacidades WMS con Python 3.7">
<correction mariadb-10.3 "Nueva versión «estable» del proyecto original; correcciones de seguridad [CVE-2019-2737 CVE-2019-2739 CVE-2019-2740 CVE-2019-2758 CVE-2019-2805]; corrige fallo de segmentación en acceso a 'information_schema'; renombra 'mariadbcheck' como 'mariadb-check'">
<correction musescore "Inhabilita la funcionalidad webkit">
<correction ncbi-tools6 "Empaquetado sin data/UniVec.* no libres">
<correction ncurses "Elimina <q>rep</q> de las descripciones de terminal («terminfo descriptions») xterm-new y derivadas">
<correction netdata "Elimina Google Analytics de la documentación generada; opta por no enviar estadísticas anónimas; elimina el botón <q>sign in</q>">
<correction newsboat "Corrige problema de «uso tras liberar»">
<correction nextcloud-desktop "Añade dependencia de nextcloud-desktop-common con nextcloud-desktop-cmd, que faltaba">
<correction node-lodash "Corrige contaminación de prototipo [CVE-2019-10744]">
<correction node-mixin-deep "Corrige problema de contaminación de prototipo">
<correction nss "Corrige problemas de seguridad [CVE-2019-11719 CVE-2019-11727 CVE-2019-11729]">
<correction nx-libs "Corrige varias fugas de contenido de la memoria">
<correction open-infrastructure-compute-tools "Corrige arranque del contenedor">
<correction open-vm-tools "Gestiona correctamente versiones de SO de la forma <q>X</q> en lugar de <q>X.Y</q>">
<correction openldap "Limita rootDN proxyauthz a sus propias bases de datos [CVE-2019-13057]; hace cumplir la sentencia ACL sasl_ssf en cada conexión [CVE-2019-13565]; corrige el fallo por el que slapo-rwm no liberaba el filtro original cuando el filtro reescrito era inválido">
<correction osinfo-db "Añade información de buster 10.0; corrige las URL para descargar stretch; corrige el nombre del parámetro usado para establecer el nombre completo al generar un fichero con las respuestas preestablecidas">
<correction osmpbf "Recompilado con protobuf 3.6.1">
<correction pam-u2f "Corrige gestión insegura de fichero de depuración [CVE-2019-12209]; corrige filtración del descriptor de fichero de depuración [CVE-2019-12210]; corrige acceso fuera de límites; corrige fallo de segmentación posterior a un error de asignación de un área de memoria">
<correction passwordsafe "Instala ficheros para traducción a otros idiomas en el directorio correcto">
<correction piuparts "Actualiza configuraciones para la versión buster; corrige errores espurios al eliminar paquetes cuyo nombre termina en '+'; genera nombres independientes de ficheros comprimidos para las jaulas chroot con --merged-usr">
<correction postgresql-common "Corrige <q>pg_upgradecluster a partir de postgresql-common 200, 200+deb10u1, 201 y 202 corrompe el valor de configuración data_directory cuando se utiliza *dos veces* para actualizar un cluster (por ejemplo, 9.6 -&gt; 10 -&gt; 11)</q>">
<correction pulseaudio "Corrige el restablecimiento del estado de silencio («mute state»)">
<correction puppet-module-cinder "Corrige intentos de escribir en /etc/init">
<correction python-autobahn "Corrige dependencias de compilación con pyqrcode">
<correction python-django "Nueva publicación de seguridad del proyecto original [CVE-2019-12781]">
<correction raspi3-firmware "Añade soporte para Raspberry Pi Compute Module 3 (CM3), Raspberry Pi Compute Module 3 Lite y Raspberry Pi Compute Module IO Board V3">
<correction reportbug "Actualiza nombres de versión, en línea con la publicación de buster; habilita de nuevo las solicitudes stretch-pu; corrige caídas con búsqueda de paquete / versión; añade dependencia con sensible-utils, que faltaba">
<correction ruby-airbrussh "No lanza una excepción con salida SSH UTF-8 inválida">
<correction sdl-image1.2 "Corrige desbordamientos de memoria [CVE-2019-5052 CVE-2019-7635] y acceso fuera de límites [CVE-2019-12216 CVE-2019-12217 CVE-2019-12218 CVE-2019-12219 CVE-2019-12220 CVE-2019-12221 CVE-2019-12222 CVE-2019-5051]">
<correction sendmail "sendmail-bin.postinst, initscript: permite que start-stop-daemon coincida en fichero de pid («pidfile») y ejecutable; sendmail-bin.prerm: detiene sendmail antes de eliminar las alternativas">
<correction slirp4netns "Nueva versión «estable» del proyecto original con correcciones de seguridad: comprueba el resultado de sscanf al emular ident [CVE-2019-9824]; corrige desbordamiento de memoria dinámica («heap») en la libslirp incluida [CVE-2019-14378]">
<correction systemd "Network: corrige error al activar una interfaz con el núcleo Linux 5.2; ask-password: evita desbordamiento de memoria al leer el anillo de claves; network: comportamiento más elegante cuando se ha inhabilitado IPv6">
<correction tzdata "Nueva versión del proyecto original">
<correction unzip "Corrige problemas de bombas zip [CVE-2019-13232]">
<correction usb.ids "Actualición rutinaria de los ID USB">
<correction warzone2100 "Corrige un fallo de segmentación al albergar un juego multijugador">
<correction webkit2gtk "Nueva versión «estable» del proyecto original; deja de requerir CPU que soporte SSE2">
<correction win32-loader "Recompilado contra paquetes actuales, especialmente debian-archive-keyring; corrige error de compilación al exigir el uso de una configuración regional («locale») POSIX">
<correction xymon "Corrige varios problemas de seguridad (exclusivos del servidor) [CVE-2019-13273 CVE-2019-13274 CVE-2019-13451 CVE-2019-13452 CVE-2019-13455 CVE-2019-13484 CVE-2019-13485 CVE-2019-13486]">
<correction yubikey-personalization "Adapta precauciones de seguridad adicionales">
<correction z3 "No da al SONAME de libz3java.so el valor libz3.so.4">
</table>


<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución «estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas
actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2019 4477 zeromq3>
<dsa 2019 4478 dosbox>
<dsa 2019 4479 firefox-esr>
<dsa 2019 4480 redis>
<dsa 2019 4481 ruby-mini-magick>
<dsa 2019 4482 thunderbird>
<dsa 2019 4483 libreoffice>
<dsa 2019 4484 linux>
<dsa 2019 4484 linux-signed-i386>
<dsa 2019 4484 linux-signed-arm64>
<dsa 2019 4484 linux-signed-amd64>
<dsa 2019 4486 openjdk-11>
<dsa 2019 4488 exim4>
<dsa 2019 4489 patch>
<dsa 2019 4490 subversion>
<dsa 2019 4491 proftpd-dfsg>
<dsa 2019 4493 postgresql-11>
<dsa 2019 4494 kconfig>
<dsa 2019 4495 linux-signed-amd64>
<dsa 2019 4495 linux-signed-arm64>
<dsa 2019 4495 linux>
<dsa 2019 4495 linux-signed-i386>
<dsa 2019 4496 pango1.0>
<dsa 2019 4498 python-django>
<dsa 2019 4499 ghostscript>
<dsa 2019 4501 libreoffice>
<dsa 2019 4502 ffmpeg>
<dsa 2019 4503 golang-1.11>
<dsa 2019 4504 vlc>
<dsa 2019 4505 nginx>
<dsa 2019 4507 squid>
<dsa 2019 4508 h2o>
<dsa 2019 4509 apache2>
<dsa 2019 4510 dovecot>
</table>


<h2>Paquetes eliminados</h2>

<p>Se han eliminado los paquetes listados a continuación por circunstancias ajenas a nosotros:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction pump "Sin desarrollo activo; problemas de seguridad">
<correction rustc "Elimina material de rust-doc desactualizado">

</table>

<h2>Instalador de Debian</h2>
<p>Se ha actualizado el instalador para incluir las correcciones incorporadas
por esta nueva versión en la distribución «estable».</p>

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «estable» actual:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Actualizaciones propuestas a la distribución «estable»:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Información sobre la distribución «estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>


