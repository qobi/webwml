#use wml::debian::template title="Debian bullseye -- Guía de instalación" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#use wml::debian::translation-check translation="a4056e907e44ae14172feaedc8e3e0bb596aa260"

<if-stable-release release="stretch">
<p>Esta es una <strong>versión beta</strong> de la guía de instalación para Debian
10, nombre en clave buster, que aún no se ha publicado. La información
que se presenta aquí podría estar desactualizada y/o ser inexacta debido a cambios en
el instalador. Podría interesarle la
<a href="../stretch/installmanual">guía de instalación de Debian
9, nombre en clave stretch</a>, que es la última versión publicada de
Debian, o la <a href="https://d-i.debian.org/manual/">versión de
los desarrolladores de la guía de instalación</a>, que es la versión más reciente
de este documento.</p>
</if-stable-release>

<if-stable-release release="buster">
<p>Esta es una <strong>versión beta</strong> de la guía de instalación para Debian
11, nombre en clave bullseye, que aún no se ha publicado. La información
que se presenta aquí podría estar desactualizada y/o ser inexacta debido a cambios en
el instalador. Podría interesarle la
<a href="../buster/installmanual">guía de instalación de Debian
10, nombre en clave buster</a>, que es la última versión publicada de
Debian, o la <a href="https://d-i.debian.org/manual/">versión de
los desarrolladores de la guía de instalación</a>, que es la versión más reciente
de este documento.</p>
</if-stable-release>

<p>Las instrucciones de instalación, junto con los ficheros que se pueden descargar,
están disponibles para cada una de las arquitecturas soportadas:</p>

<ul>
<:= &permute_as_list('', 'Guía de instalación'); :>
</ul>

<p>Si ha configurado adecuadamente las opciones de localización de su
navegador, podrá usar el enlace anterior para acceder automáticamente a la versión
HTML correcta &mdash; consulte la información sobre <a href="$(HOME)/intro/cn">negociación de contenido</a>.
Si no es así, elija la arquitectura exacta, idioma, y formato que desee
en la tabla siguiente.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Arquitectura</strong></th>
  <th align="left"><strong>Formato</strong></th>
  <th align="left"><strong>Idiomas</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'install', langs => \%langsinstall,
			   formats => \%formats, arches => \@arches,
			   html_file => 'index', namingscheme => sub {
			   $_[2] eq html ? "$_[0].$_[1].$_[2]" : "$_[0].$_[2].$_[1]" } ); :>
</table>
</div>
