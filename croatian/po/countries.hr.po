msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2008-08-16 14:05+0200\n"
"Last-Translator: Josip Rodin\n"
"Language-Team: Croatian\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/countries.wml:111
msgid "United Arab Emirates"
msgstr "Ujedinjeni Arapski Emirati"

#: ../../english/template/debian/countries.wml:114
msgid "Albania"
msgstr "Albanija"

#: ../../english/template/debian/countries.wml:117
msgid "Armenia"
msgstr "Armenija"

#: ../../english/template/debian/countries.wml:120
msgid "Argentina"
msgstr "Argentina"

#: ../../english/template/debian/countries.wml:123
msgid "Austria"
msgstr "Austrija"

#: ../../english/template/debian/countries.wml:126
msgid "Australia"
msgstr "Australija"

#: ../../english/template/debian/countries.wml:129
msgid "Bosnia and Herzegovina"
msgstr "Bosna i Hercegovina"

#: ../../english/template/debian/countries.wml:132
msgid "Bangladesh"
msgstr "Bangladeš"

#: ../../english/template/debian/countries.wml:135
msgid "Belgium"
msgstr "Belgija"

#: ../../english/template/debian/countries.wml:138
msgid "Bulgaria"
msgstr "Bugarska"

#: ../../english/template/debian/countries.wml:141
msgid "Brazil"
msgstr "Brazil"

#: ../../english/template/debian/countries.wml:144
msgid "Bahamas"
msgstr "Bahami"

#: ../../english/template/debian/countries.wml:147
msgid "Belarus"
msgstr "Bjelorusija"

#: ../../english/template/debian/countries.wml:150
msgid "Canada"
msgstr "Kanada"

#: ../../english/template/debian/countries.wml:153
msgid "Switzerland"
msgstr "Švicarska"

#: ../../english/template/debian/countries.wml:156
msgid "Chile"
msgstr "Čile"

#: ../../english/template/debian/countries.wml:159
msgid "China"
msgstr "Kina"

#: ../../english/template/debian/countries.wml:162
msgid "Colombia"
msgstr "Kolumbija"

#: ../../english/template/debian/countries.wml:165
msgid "Costa Rica"
msgstr "Kostarika"

#: ../../english/template/debian/countries.wml:168
msgid "Czech Republic"
msgstr "Češka"

#: ../../english/template/debian/countries.wml:171
msgid "Germany"
msgstr "Njemačka"

#: ../../english/template/debian/countries.wml:174
msgid "Denmark"
msgstr "Danska"

#: ../../english/template/debian/countries.wml:177
msgid "Dominican Republic"
msgstr "Dominikanska Republika"

#: ../../english/template/debian/countries.wml:180
msgid "Algeria"
msgstr "Alžir"

#: ../../english/template/debian/countries.wml:183
msgid "Ecuador"
msgstr "Ekvador"

#: ../../english/template/debian/countries.wml:186
msgid "Estonia"
msgstr "Estonija"

#: ../../english/template/debian/countries.wml:189
msgid "Egypt"
msgstr "Egipat"

#: ../../english/template/debian/countries.wml:192
msgid "Spain"
msgstr "Španjolska"

#: ../../english/template/debian/countries.wml:195
msgid "Ethiopia"
msgstr "Etiopija"

#: ../../english/template/debian/countries.wml:198
msgid "Finland"
msgstr "Finska"

#: ../../english/template/debian/countries.wml:201
msgid "Faroe Islands"
msgstr "Farski otoci"

#: ../../english/template/debian/countries.wml:204
msgid "France"
msgstr "Francuska"

#: ../../english/template/debian/countries.wml:207
msgid "United Kingdom"
msgstr "Velika Britanija"

#: ../../english/template/debian/countries.wml:210
msgid "Grenada"
msgstr "Grenada"

#: ../../english/template/debian/countries.wml:213
msgid "Georgia"
msgstr "Gruzija"

#: ../../english/template/debian/countries.wml:216
msgid "Greenland"
msgstr "Grenland"

#: ../../english/template/debian/countries.wml:219
msgid "Greece"
msgstr "Grčka"

#: ../../english/template/debian/countries.wml:222
msgid "Guatemala"
msgstr "Gvatemala"

#: ../../english/template/debian/countries.wml:225
msgid "Hong Kong"
msgstr "Hong Kong"

#: ../../english/template/debian/countries.wml:228
msgid "Honduras"
msgstr "Honduras"

#: ../../english/template/debian/countries.wml:231
msgid "Croatia"
msgstr "Hrvatska"

#: ../../english/template/debian/countries.wml:234
msgid "Hungary"
msgstr "Mađarska"

#: ../../english/template/debian/countries.wml:237
msgid "Indonesia"
msgstr "Indonezija"

#: ../../english/template/debian/countries.wml:240
msgid "Iran"
msgstr "Iran"

#: ../../english/template/debian/countries.wml:243
msgid "Ireland"
msgstr "Irska"

#: ../../english/template/debian/countries.wml:246
msgid "Israel"
msgstr "Izrael"

#: ../../english/template/debian/countries.wml:249
msgid "India"
msgstr "Indija"

#: ../../english/template/debian/countries.wml:252
msgid "Iceland"
msgstr "Island"

#: ../../english/template/debian/countries.wml:255
msgid "Italy"
msgstr "Italija"

#: ../../english/template/debian/countries.wml:258
msgid "Jordan"
msgstr "Jordan"

#: ../../english/template/debian/countries.wml:261
msgid "Japan"
msgstr "Japan"

#: ../../english/template/debian/countries.wml:264
msgid "Kenya"
msgstr "Kenija"

#: ../../english/template/debian/countries.wml:267
#, fuzzy
msgid "Kyrgyzstan"
msgstr "Kazahstan"

#: ../../english/template/debian/countries.wml:270
msgid "Korea"
msgstr "Koreja"

#: ../../english/template/debian/countries.wml:273
msgid "Kuwait"
msgstr "Kuvajt"

#: ../../english/template/debian/countries.wml:276
msgid "Kazakhstan"
msgstr "Kazahstan"

#: ../../english/template/debian/countries.wml:279
msgid "Sri Lanka"
msgstr "Šri Lanka"

#: ../../english/template/debian/countries.wml:282
msgid "Lithuania"
msgstr "Litva"

#: ../../english/template/debian/countries.wml:285
msgid "Luxembourg"
msgstr "Luksemburg"

#: ../../english/template/debian/countries.wml:288
msgid "Latvia"
msgstr "Latvija"

#: ../../english/template/debian/countries.wml:291
msgid "Morocco"
msgstr "Maroko"

#: ../../english/template/debian/countries.wml:294
msgid "Moldova"
msgstr ""

#: ../../english/template/debian/countries.wml:297
msgid "Montenegro"
msgstr "Crna Gora"

#: ../../english/template/debian/countries.wml:300
msgid "Madagascar"
msgstr "Madagaskar"

#: ../../english/template/debian/countries.wml:303
msgid "Macedonia, Republic of"
msgstr "Makedonija"

#: ../../english/template/debian/countries.wml:306
msgid "Mongolia"
msgstr ""

#: ../../english/template/debian/countries.wml:309
msgid "Malta"
msgstr "Malta"

#: ../../english/template/debian/countries.wml:312
msgid "Mexico"
msgstr "Meksiko"

#: ../../english/template/debian/countries.wml:315
msgid "Malaysia"
msgstr "Malezija"

#: ../../english/template/debian/countries.wml:318
msgid "New Caledonia"
msgstr "Nova Kaledonija"

#: ../../english/template/debian/countries.wml:321
msgid "Nicaragua"
msgstr "Nikaragva"

#: ../../english/template/debian/countries.wml:324
msgid "Netherlands"
msgstr "Nizozemska"

#: ../../english/template/debian/countries.wml:327
msgid "Norway"
msgstr "Norveška"

#: ../../english/template/debian/countries.wml:330
msgid "New Zealand"
msgstr "Novi Zeland"

#: ../../english/template/debian/countries.wml:333
msgid "Panama"
msgstr "Panama"

#: ../../english/template/debian/countries.wml:336
msgid "Peru"
msgstr "Peru"

#: ../../english/template/debian/countries.wml:339
msgid "French Polynesia"
msgstr "Francuska Polinezija"

#: ../../english/template/debian/countries.wml:342
msgid "Philippines"
msgstr "Filipini"

#: ../../english/template/debian/countries.wml:345
msgid "Pakistan"
msgstr "Pakistan"

#: ../../english/template/debian/countries.wml:348
msgid "Poland"
msgstr "Poljska"

#: ../../english/template/debian/countries.wml:351
msgid "Portugal"
msgstr "Portugal"

#: ../../english/template/debian/countries.wml:354
msgid "Réunion"
msgstr ""

#: ../../english/template/debian/countries.wml:357
msgid "Romania"
msgstr "Rumunjska"

#: ../../english/template/debian/countries.wml:360
msgid "Serbia"
msgstr "Srbija"

#: ../../english/template/debian/countries.wml:363
msgid "Russia"
msgstr "Rusija"

#: ../../english/template/debian/countries.wml:366
msgid "Saudi Arabia"
msgstr "Saudijska Arabija"

#: ../../english/template/debian/countries.wml:369
msgid "Sweden"
msgstr "Švedska"

#: ../../english/template/debian/countries.wml:372
msgid "Singapore"
msgstr "Singapur"

#: ../../english/template/debian/countries.wml:375
msgid "Slovenia"
msgstr "Slovenija"

#: ../../english/template/debian/countries.wml:378
msgid "Slovakia"
msgstr "Slovačka"

#: ../../english/template/debian/countries.wml:381
msgid "El Salvador"
msgstr "Salvador"

#: ../../english/template/debian/countries.wml:384
msgid "Thailand"
msgstr "Tajland"

#: ../../english/template/debian/countries.wml:387
msgid "Tajikistan"
msgstr "Tadžikistan"

#: ../../english/template/debian/countries.wml:390
msgid "Tunisia"
msgstr "Tunis"

#: ../../english/template/debian/countries.wml:393
msgid "Turkey"
msgstr "Turska"

#: ../../english/template/debian/countries.wml:396
msgid "Taiwan"
msgstr "Tajvan"

#: ../../english/template/debian/countries.wml:399
msgid "Ukraine"
msgstr "Ukrajina"

#: ../../english/template/debian/countries.wml:402
msgid "United States"
msgstr "Sjedinjene Države"

#: ../../english/template/debian/countries.wml:405
msgid "Uruguay"
msgstr "Urugvaj"

#: ../../english/template/debian/countries.wml:408
msgid "Uzbekistan"
msgstr "Uzbekistan"

#: ../../english/template/debian/countries.wml:411
msgid "Venezuela"
msgstr "Venecuela"

#: ../../english/template/debian/countries.wml:414
msgid "Vietnam"
msgstr "Vijetnam"

#: ../../english/template/debian/countries.wml:417
msgid "Vanuatu"
msgstr "Vanuatu"

#: ../../english/template/debian/countries.wml:420
msgid "South Africa"
msgstr "Južna Afrika"

#: ../../english/template/debian/countries.wml:423
msgid "Zimbabwe"
msgstr "Zimbabve"

#~ msgid "Great Britain"
#~ msgstr "Velika Britanija"
