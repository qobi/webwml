#use wml::debian::template title="데비안 개발자 코너" BARETITLE="true"
#use wml::debian::translation-check translation="2a91a8cd41f14166dc4ef23023d68bc2825268ff" maintainer="Sebul"

<p>이 페이지의 정보는, 공개적이지만, 주로 데비안 개발자에게 흥미로울 겁니다.
</p>

<ul class="toc">
<li><a href="#basic">기본</a></li>
<li><a href="#packaging">패키징</a></li>
<li><a href="#workinprogress">진행 중</a></li>
<li><a href="#projects">프로젝트</a></li>
<li><a href="#miscellaneous">기타</a></li>
</ul>

<div id="main">
  <div class="cardleft" id="basic">
  <h2>기본</h2>
      <div>
      <dl>
        <dt><a href="$(HOME)/intro/organization">데비안 조직</a></dt>

        <dd>데비안은 많은 액세스 포인트가 있으며, 관련된 사람이 많습니다.
이 페이지는 데비안의 특정 면에 대해 누가 연락해야 되는지 설명하고,
누가 응답할 것인지 이야기합니다.
        </dd>

        <dt>사람들</dt>
        <dd>데비안은 세계에 흩어진 많은 사람들에 의해 협동적으로 만들어집니다.
<em>패키징 작업</em>은 일반적으로 
<a href="https://wiki.debian.org/DebianDeveloper">데비안 개발자
        (DD)</a>(데비안 프로젝트 정회원)와 <a
        href="https://wiki.debian.org/DebianMaintainer">데비안 유지관리자
        (DM)</a>가 함께 기여합니다.
여기서 <a href="people">데비안 개발자 목록</a> 그리고 <a
        href="https://nm.debian.org/public/people/dm_all">데비안 유지관리자 목록</a>, 
그들이 유지관리하는 패키지를 모두 찾을 수 있습니다.
<p>또
<a href="developers.loc">데비안 개발자의 세계지도</a>와 <a 
href="https://gallery.debconf.org/">이미지 갤러리
</a>를 볼 수 있습니다..
        </p>
        </dd>

        <dt><a href="join/">데비안 가입</a></dt>

        <dd>데비안 프로젝트는 자원봉사자로 구성되며,  우리는 보통
기술적 지식, 소프트웨어에 대한 흥미, 여유시간을 가진 새 개발자를 찾고 있습니다.
여러분도 데비안을 도울 수 있으며, 위에 연결된 페이지를 보면 됩니다.
        </dd>

        <dt><a href="https://db.debian.org/">개발자 데이터베이스</a></dt>
        <dd>데이터베이스는 모두가 접근 가능한 기본 데이터를 포함하며,
좀더 사적인 데이터는 다른 개발자들만 볼 수 있습니다. 
로그인 하려면 <a href="https://db.debian.org/">SSL 버전</a>을 써서 접근합니다.

        <p>데이터베이스를 써서 다음 목록을 볼 수 있습니다
        <a href="https://db.debian.org/machines.cgi">프로젝트 기계</a>,
        <a href="extract_key">개발자 GPG 키 얻기</a>,
        <a href="https://db.debian.org/password.html">암호 바꾸기</a>
        또는 데비안 계정을 위해 <a href="https://db.debian.org/forward.html">메일포워딩 어떻게 설정하는지 배우기</a>
</p>

        <p>데비안 기계를 사용하려면
<a href="dmup">데비안 기계 사용 정책</a>을 읽었는지 확인하세요.</p>
        </dd>

        <dt><a href="constitution">데비안 헌법</a></dt>
        <dd>조직에 가장 중요한 문서로, 프로젝트에서 공식적 결정을 위한 조직 구조를 설명함.
        </dd>

        <dt><a href="$(HOME)/vote/">투표 정보</a></dt>
        <dd>리더를 어떻게 뽑는지, 로고를 고르고, 일반적으로 우리가 어떻게 선거하는지 우리가 알고 싶은 모든 것.
        </dd>
     </dl>

# this stuff is really not devel-only
     <dl>
        <dt><a href="$(HOME)/releases/">릴리스</a></dt>

        <dd>이것은 과거와 현재의 릴리스 목록이며, 그중 일부는 별도 웹 페이지에 자세한 정보를 갖고 있음.

        <p>여러분은 직접 <a href="$(HOME)/releases/stable/">안정 릴리스</a>와
        <a href="$(HOME)/releases/testing/">테스트 배포본</a> 웹 페이지에 갈 수 있습니다.</p>
        </dd>

        <dt><a href="$(HOME)/ports/">다른 아키텍처</a></dt>

        <dd>
데비안은 여러 종류의 컴퓨터에서 실행되며(Intel 호환성은 단지 <em>처음</em> 종류뿐),
		우리 &lsquo;ports&rsquo;의 관리자들은 몇 가지 유용한 웹 페이지를 가지고 있습니다.
		보세요, 당신은 당신 자신을 위해 또 다른 이상한 이름을 가진 금속 조각을 얻고 싶을수도 있습니다.	
	</dd>
      </dl>
      </div>

  </div>

  <div class="cardright" id="packaging">
     <h2>패키징</h2>
     <div>

      <dl>
        <dt><a href="$(DOC)/debian-policy/">데비안 정책 매뉴얼</a></dt>
        <dd>
 이 매뉴얼은 데비안 배포에 대한 정책 요구 사항을 설명합니다.
여기에는 데비안 아카이브의 구조와 내용,
운영 체제의 몇 가지 설계 문제 및 각 패키지가 배포에 포함되기 위해 충족해야 하는 기술적 요구 사항을 포함합니다.
       
        <p>간단히, 여러분은 그것을 읽을 <strong>필요</strong>가 있습니다.</p>
        </dd>
      </dl>

      <p>여러분이 관심 가질 정책에 관련된 여러 문서가 아래와 같이 있습니다:</p>
      <ul>
        <li><a href="https://wiki.linuxfoundation.org/lsb/fhs/">파일 시스템 계층 표준</a> (FHS)
        <br />FHS는 물건을 넣어야 하는 디렉터리(또는 파일)의 목록이며, 그와의 호환성은 정책 3.x에 필요하다.
		</li>
        <li><a href="$(DOC)/packaging-manuals/build-essential">build-essential packages</a> 목록
        <br />build-essential 패키지는 어떤 패키지 또는 패키지의 <code>Build-Depends</code> 라인에 포함할 필요 없는 
		패키지 세트를 빌드하기 전에 필요한 패키지입니다.
		</li>
        <li><a href="$(DOC)/packaging-manuals/menu-policy/">메뉴 시스템</a>
        <br />정상적인 작동을 위해 특별한 명령 줄 인수를 전달할 필요 없는 인터페이스가 있는 프로그램에는 
		메뉴 항목이 등록되어 있어야합니다.
		<a href="$(DOC)/packaging-manuals/menu.html/">menu system documentation</a>도 확인하세요.
		</li>
        <li><a href="$(DOC)/packaging-manuals/debian-emacs-policy">Emacs 정책</a>
        <br />이맥스와 관련된 패키지는 자체 하위 정책 문서를 준수할 것으로 기대된다.
		</li>
        <li><a href="$(DOC)/packaging-manuals/java-policy/">Java 정책</a>
        <br />Java 관련 패키지에 대해 위에 대해 제안된 등가물.</li>
        <li><a href="$(DOC)/packaging-manuals/perl-policy/">Perl 정책</a>
        <br />Perl 패키징에 관한 모든 것을 다루는 하위 정책.
		</li>
        <li><a href="$(DOC)/packaging-manuals/python-policy/">Python 정책</a>
        <br />Python 패키지에 관한 모든 것을 다루는 하위 정책.
		</li>
	
        <li><a href="$(DOC)/packaging-manuals/debconf_specification.html">Debconf 명세</a>
        <br />"debconf"에 대한 규격.</li>
	<li><a href="https://www.debian.org/doc/manuals/dbapp-policy/">데이터베이스 응응프로그램 정책</a> (초안)
	<br />데이터베이스 응용 프로그램 패키지에 대한 지침 및 모범 사례 모음.</li>
	<li><a href="https://tcltk-team.pages.debian.net/policy-html/tcltk-policy.html/">Tcl/Tk 정책</a> (초안)
	<br />Tcl/Tk 패키징 관련 모든 내용을 다루는 하위 정책.</li>
	<li><a
	href="https://people.debian.org/~lbrenta/debian-ada-policy.html">Ada를 위한 데비안 정책</a>
	<br />Ada 패키징 관련 모든 내용을 다루는 하위 정책.</li>
      </ul>

      <p><a href="https://bugs.debian.org/debian-policy">
      proposed updates to Policy</a>도 보세요.</p>

      <p>이전 패키징 매뉴얼은 대부분 정책 매뉴얼의 최신 버전에 통합되어 있다는 점을 유의하세요.
	  </p>

      <dl>
        <dt><a href="$(DOC)/manuals/developers-reference/">개발자 레퍼런스</a></dt>

        <dd>
이 문서의 목적은 데비안 개발자에게 권장 절차와 사용 가능한 자원에 대한 개요를 제공하는 겁니다.
또 다른 필독서.
        </dd>

        <dt><a href="$(DOC)/manuals/maint-guide/">새 유지관리자 안내서</a></dt>

        <dd>
이 문서는 데비안 패키지의 구축을 공통어로 기술하고 있으며, 작업 예제로 잘 다루어져 있습니다.
만약 당신이 개발자(패키저)를 원한다면, 당신은 이것을 가장 확실히 읽고 싶을 겁니다.
        </dd>
      </dl>
      </div>

  </div>

  <div class="card" id="workinprogress">
      <h2>작업&nbsp;진행&nbsp;중</h2>
      <div>

	<dl>
        <dt><a href="testing">테스트 배포본</a></dt>
        <dd>
		&lsquo;testing&rsquo; 배포는 패키지를 다음에 데비안이 출시할 때 그것들을 고려하기 위해 필요한 곳입이다.
        </dd>

        <dt><a href="https://bugs.debian.org/release-critical/">릴리스 심각한 버그</a></dt>

        <dd>
		이것은 "테스트" 배포에서 패키지를 제거할 수 있는 버그 목록이며,
		또는 어떤 경우에는 배포 릴리스가 지연되는 경우도 있습니다.
     	목록에서 &lsquo;심각&rsquo; 이상의 심각도를 가진 버그 리포트 - 가능한 한 빨리 패키지에 대한 버그를 수정하세요.		
        </dd>

        <dt><a href="$(HOME)/Bugs/">버그 추적 시스템</a></dt>
        <dd>
        Debian Bug Tracking System(BTS)은 버그를 보고하고, 토론하고, 고치는 곳.
		데비안의 거의 모든 부분의 문제들에 대한 보고는 여기서 환영한다.
		BTS는 사용자와 개발자 모두에게 쓸모 있습니다.		
        </dd>

        <dt>패키지 개론, 개발자 관점에서</dt>
        <dd>
<a href="https://qa.debian.org/developer.php">package information</a>
        및 <a href="https://packages.qa.debian.org/">package tracking</a> 웹 페이지는
메인테이너에게 값진 정보를 줍니다.
        </dd>

        <dt><a href="$(DOC)/manuals/developers-reference/resources#pkg-tracker">
        패키지 추적기</a></dt>
        <dd>
다른 패키지의 최신 상태를 유지하고자 하는 개발자를 위해 패키지 추적기는 가입된 패키지와 관련된 업로드 및 설치를 위해 BTS 메일 사본과 알림을 전송하는 서비스에 가입(이메일을 통해)할 수 있도록 한다.
        </dd>

        <dt><a href="wnpp/">도움이 필요한 패키지</a></dt>
        <dd>
Work-Needing and Possible Packages(WNPP)는 새로운 정비사가 필요한 데비안 패키지의 목록이며, 또한 데비안에 아직 포함되지 않은 패키지의 목록임.
패키지를 만들거나, 받아들이거나 지우려면 해당 패키지를 확인하시오.
        </dd>
### 20200403
        <dt><a href="$(DOC)/manuals/developers-reference/resources.html#incoming-system">\
            incoming 시스템</a></dt>
        <dd>
		새 패키지는 내부 아카이브 서버에 있는 "Incoming" 시스템 안으로 들어갑니다.
수락된 패키지는 거의 즉시 <a href="https://incoming.debian.org/">HTTP</a>를 통해 이용할 수 있으며,
하루에 4번 <a href="$(HOME)/mirror/">mirrors</a>로 전파된다.
        <br />
        <strong>주</strong>: Incoming의 특성상, 미러링하는 건 권장하지 않음.
        </dd>

        <dt><a href="https://lintian.debian.org/">Lintian 보고</a></dt>

        <dd>
        <a href="https://packages.debian.org/unstable/devel/lintian">
        Lintian</a>은 패키지가 정책에 부합하는지 확인하는 프로그램이다.
업로드 전에  이걸 써야 함;
유통의 모든 패키지에 대해 앞서 언급한 페이지에 보고서가 있다.
        </dd>

        <dt><a href="https://wiki.debian.org/HelpDebian">데비안 돕기</a></dt>
        <dd>
데비안 위키는 개발자와 다른 기여자를 위한 조언을 모은다.
        </dd>

        <dt><a href="$(DOC)/manuals/developers-reference/resources#s4.6.4">\
            실험적 배포본</a></dt>
        <dd>
		<em>experimental</em> 배포는 고도로 실험적인 소프트웨어의 임시 준비 구역으로 사용된다.
        <a href="https://packages.debian.org/experimental/"><em>experimental</em>에서 온 패키지</a>는 
		<em>unstable</em>을 어떻게 쓰는지 알 때만 쓰세요.
        </dd>
      </dl>
      </div>

  </div>
  <div class="card" id="projects">
     <h2>프로젝트</h2>
     <div>

      <p>
데비안은 큰 그룹이며, 이와 같이 여러 내부 그룹과 프로젝트로 구성되어 있다.
시간순으로 정렬된 웹 페이지:	  
	  </p>
      <ul>
          <li><a href="website/">데비안 웹 페이지</a></li>
          <li><a href="https://ftp-master.debian.org/">데비안 아카이브</a></li>
          <li><a href="$(DOC)/ddp">데비안 문서 프로젝트 (DDP)</a></li>
          <li><a href="https://qa.debian.org/">품질 보증</a>
              그룹</li>
          <li><a href="$(HOME)/CD/">데비안 CD 이미지</a></li>
          <li><a href="https://wiki.debian.org/Keysigning">키 사이닝
              조정 페이지</a></li>
          <li><a href="https://wiki.debian.org/DebianIPv6">데비안 IPv6
프로젝트</a></li>
          <li><a href="buildd/">Auto-builder network</a> 및 그 <a href="https://buildd.debian.org/"> 로그 작성</a></li>
<li><a href="$(HOME)/international/l10n/ddtp">데비안 설명 번역 프로젝트 (Debian Description Translation Project, DDTP)</a></li>
	  <li><a href="debian-installer/">데비안 설치관리자</a></li>
	  <li><a href="debian-live/">데비안 라이브</a></li>
	  <li><a href="$(HOME)/women/">데비안 여성</a></li>
	  <li><a href="$(HOME)/blends/">데비안 순수</a></li>
	</ul>
	</div>

  </div>

  <div class="card" id="miscellaneous">
      <h2>기타</h2>
      <div>

      <p>관련 링크:</p>
      <ul>
	  <li>컨퍼런스 이야기 <a href="https://debconf-video-team.pages.debian.net/videoplayer/">기록</a>.</li>
        <li><a href="passwordlessssh">ssh 설정해서 암호 안 물어보게</a>.</li>
        <li>어떻게 <a href="$(HOME)/MailingLists/HOWTO_start_list">새 메일링 리스트를 요청</a>하는지.</li>
        <li><a href="$(HOME)/mirror/">데비안 미러</a>에 대한 정보.</li>
        <li><a href="https://qa.debian.org/data/bts/graphs/all.png">모든 버그 그래프</a>.</li>
	<li><a href="https://ftp-master.debian.org/new.html">데비안에 들어가길 기다리는 새 패키지</a> (NEW queue).</li>
        <li><a href="https://packages.debian.org/unstable/main/newpkg">최근 7일 새 데비안 패키지</a>.</li>
        <li><a href="https://ftp-master.debian.org/removals.txt">데비안에서 없어진 패키지</a>.</li>
        </ul>

      </div>

  </div>
</div>
