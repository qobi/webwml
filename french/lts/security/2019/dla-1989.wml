#use wml::debian::translation-check translation="447b39efa58ccade07c201536302be825577e53f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation des privilèges, un déni de service, ou une
fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0154">CVE-2019-0154</a>

<p>Intel a découvert que leurs processeurs graphiques de huitième et neuvième
génération, lorsqu'ils lisent certains registres tandis que le processeur
graphique est dans un état de faible puissance, peuvent provoquer un blocage
système. Un utilisateur local autorisé à utiliser le processeur graphique peut
se servir de cela pour un déni de service.</p>

<p>Cette mise à jour atténue le problème grâce à des modifications dans le
pilote des cartes graphiques i915.</p>

<p>Les puces affectées (de huitième et neuvième génération) sont listées sur la page
<a href="https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Gen8">en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11135">CVE-2019-11135</a>

<p>Sur les processeurs Intel prenant en charge la mémoire transactionnelle
(TSX), une transaction qui va être interrompue peut être poursuivie pour une
exécution spéculative, lisant des données sensibles à partir des tampons
internes et les divulguant à travers les opérations dépendantes. Intel appelle
cela « TSX Asynchronous Abort » (TAA).</p>

<p>Pour les processeurs affectés par les problèmes « Microarchitectural Data
Sampling (MDS) » précédemment publiés
(<a href="https://security-tracker.debian.org/tracker/CVE-2018-12126">CVE-2018-12126</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2018-12127">CVE-2018-12127</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2018-12130">CVE-2018-12130</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-11091">CVE-2019-11091</a>),
la mitigation existante atténue aussi ce problème.</p>

<p>Pour les processeurs qui sont vulnérables aux TAA mais pas aux MDS, cette
mise à jour désactive TSX par défaut. Cette mitigation requiert un microcode
de processeur mis à jour. Un paquet mis à jour du paquet intel-microcode (disponible
seulement dans Debian non-free) sera fourni par la DSA 4565-1. Le microcode
de processeur mis à jour peut aussi être disponible dans le cadre de la mise à jour
du microprogramme du système (« BIOS »).</p>

<p>Plus d'informations sur la mitigation sont disponibles sur la page
<a href="https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/tsx_async_abort.html">www.kernel.org/…/tsx_async_abort.html</a>
ou dans le paquet linux-doc-3.16.</p>

<p>L'explication par Intel de ce problème est disponible sur la page
<a href="https://software.intel.com/security-software-guidance/insights/deep-dive-intel-transactional-synchronization-extensions-intel-tsx-asynchronous-abort">software.intel.com/…/deep-dive-intel-transactional-synchronization-extensions-intel-tsx-asynchronous-abort</a>.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.16.76-1. Cette mise à jour inclut aussi d’autres correctifs des
mises à jour de stable par l’amont.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1989.data"
# $Id: $
