#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans PHP (PHP, acronyme récursif :
Hypertext Preprocessor), un langage de script généraliste au source libre
couramment utilisé, particulièrement adapté pour le développement web et pouvant
être incorporé dans du HTML.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10397">CVE-2016-10397</a>

<p>Un traitement incorrect de divers composants URI dans l'analyseur d’URL
pourrait être utilisé par des attaquants pour contourner des vérifications d’URL
particulières au nom d’hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11143">CVE-2017-11143</a>

<p>Une libération non valable dans la désérialisation de paramètres
booléens WDDX pourrait être utilisée par des attaquants capables d’injecter du
XML dans la désérialisation pour planter l’interpréteur PHP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11144">CVE-2017-11144</a>

<p>Le code PEM de scellage de l’extension openssl ne vérifiait pas la valeur de
renvoi de la fonction « sealing » d’OpenSSL, ce qui pourrait conduire à un
plantage de l’interpréteur PHP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11145">CVE-2017-11145</a>

<p>Le manque de vérifications de limites dans le code d’analyse de l’extension
de date timelib_meridian, pourrait être utilisé par des attaquants capables de
fournir des chaînes de date pour divulguer des informations de l’interpréteur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11147">CVE-2017-11147</a>

<p>Le gestionnaire d’archive PHAR pourrait être utilisé par des attaquants
fournissant des fichiers d’archive malveillants pour planter l’interpréteur PHP
ou, éventuellement, pour divulguer des informations à cause
d’une lecture excessive de tampon dans la fonction phar_parse_pharfile dans
ext/phar/phar.c.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 5.4.45-0+deb7u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1034.data"
# $Id: $
