#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvés dans Spamassassin, qui pourraient
conduire à l’exécution de code à distance et à une attaque de déni de service
dans certaines circonstances.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1238">CVE-2016-1238</a>

<p>Beaucoup de programmes Perl ne suppriment pas correctement le caractère « . »
(point) à la fin du tableau des répertoires include. Cela pourrait permettre à
des utilisateurs locaux d’augmenter leurs droits à l'aide d'un module de cheval de
Troie dans le répertoire de travail utilisé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15705">CVE-2017-15705</a>

<p>Une vulnérabilité de déni de service a été identifiée, existant dans
SpamAssassin d’Apache avant 3.4.2. Cette vulnérabilité survient avec certaines
balises non fermées de courriels qui font que le balisage est géré
incorrectement aboutissant à des arrêts d’analyse. Cela peut permettre à des
courriels soigneusement contrefaits, pouvant demander plus de temps d’analyse
que prévu, de conduire à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11780">CVE-2018-11780</a>

<p>Un bogue d’exécution potentielle de code à distance existe avec le greffon
PDFInfo SpamAssassin d’Apache avant 3.4.2.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11781">CVE-2018-11781</a>

<p>SpamAssassin 3.4.2 d’Apache corrige une injection de code d’utilisateur local
dans la syntaxe de règle d’élément meta.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la
version 3.4.2-0+deb8u1. L’amont recommande fortement la mise à niveau vers la
dernière version amont. Aussi nous suivons ces recommandations et rétroportons
la version publiée comme faisant partie de la publication Stretch 9.6
qui corrige aussi plusieurs bogues critiques.</p>

<p>Nous vous recommandons de mettre à jour vos paquets spamassassin.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1578.data"
# $Id: $
