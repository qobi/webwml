#use wml::debian::template title="De &ldquo;testing&rdquo;-distributie van Debian" BARETITLE=true
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="ff05f695462dd08fe524856384ed03a2dbb6763f"

<p>Voor basale informatie voor de gebruiker over de testing-distributie moet
u de <a href="$(DOC)/manuals/debian-faq/ftparchives#testing">Debian FAQ</a> raadplegen.</p>

<p>Een belangrijke opmerking, zowel voor gewone gebruikers als voor de
ontwikkelaars van testing, is dat beveiligingsupdates voor testing
<strong>niet door het veiligheidsteam beheerd worden</strong>. Raadpleeg voor
meer informatie de
<a href="../security/faq#testing">FAQ van het veiligheidsteam</a>.</p>

<p>Op deze pagina worden hoofdzakelijk aspecten van <q>testing</q> behandeld
die belangrijk zijn voor ontwikkelaars van Debian.</p>

<h2>Hoe <q>testing</q> werkt</h2>

<p>De <q>testing</q>-distributie is een automatisch gegenereerde distributie.
Ze wordt gegenereerd vanuit de <q>unstable</q>-distributie door een aantal
scripts welke pakketten die redelijk waarschijnlijk geen release-kritieke bugs
bevatten, proberen over te plaatsen. Zij doen dit op zodanige wijze dat
ervoor gezorgd wordt dat steeds voldaan kan worden aan de vereisten van
andere pakketten in testing.</p>

<p>Een (specifieke versie van een) pakket zal naar testing doorschuiven
wanneer het aan al de volgende criteria beantwoordt:</p>

<ol>
  <li>Het moet aanwezig geweest zijn in unstable gedurende 10, 5 of 2 dagen,
  afhankelijk van de dringendheid van de upload;</li>

  <li>Het moet gecompileerd en up-to-date zijn voor alle architecturen
  waarvoor het vroeger in unstable gecompileerd geweest is;</li>

  <li>Het mag geen release-kritieke bugs bevatten die nog niet aanwezig zijn
  in de huidige versie in <q>testing</q> (zie hieronder voor
  <a href="#faq">meer informatie</a>);</li>

  <li>Al zijn vereisten moeten <em>ofwel</em> beantwoord worden door reeds
  in <q>testing</q> aanwezige pakketten, <em>of</em> beantwoord kunnen worden
  door de groep pakketten die tegelijkertijd geïnstalleerd zullen worden;</li>

  <li>Het installeren van het pakket in <q>testing</q> mag geen enkel
  pakket dat momenteel in <q>testing</q> is, onklaar maken. (Zie hierna voor
  <a href="#faq">meer informatie</a>.)</li>
</ol>

<p>Een pakket dat aan de eerste drie bovenstaande vereisten beantwoordt, wordt een <q>Geldige Kandidaat</q> genoemd.</p>

<p>Het updatescript geeft weer wanneer welk pakket van <q>unstable</q> naar
<q>testing</q> zou kunnen verhuizen. Het heeft een tweevoudige uitvoer:</p>

<ul>
  <li>De <a href="https://release.debian.org/britney/update_excuses.html">\
      update excuses</a>
      [<a href="https://release.debian.org/britney/update_excuses.html.gz">\
      met gzip gecomprimeerd</a>]:
      een lijst van alle kandidaat-pakketversies en de fundamentele toestand
      van hun voortgang naar <q>testing</q>; dit is iets korter en mooier dan
  </li>
  <li>De <a href="https://release.debian.org/britney/update_output.txt">\
      update output</a>
      [<a href="https://release.debian.org/britney/update_output.txt.gz">\
      met gzip gecomprimeerd</a>]:
      de volledige, eerder onbewerkte uitvoer van de <q>testing</q>-scripts
      terwijl ze de kandidaten overlopen
  </li>
</ul>

<h2><a name="faq">Vaak gestelde/beantwoorde vragen</a></h2>

# Note to translators: these two first items are almost the same as
# https://www.debian.org/doc/manuals/developers-reference/pkgs.html#faq

<h3><q>Wat zijn release-kritieke bugs en hoe worden ze geteld?</q></h3>

<p>Alle bugs met een zekere graad van ernstigheid worden standaard beschouwd als
<em><a href="https://bugs.debian.org/release-critical/">release-kritiek</a></em>;
momenteel zijn dit bugs die gecatalogeerd worden als
<strong>critical</strong> (kritiek), <strong>grave</strong> (ernstig) of
<strong>serious</strong> (serieus).</p>

<p>Van dergelijke bugs wordt aangenomen dat ze een impact hebben op de kansen
die het pakket heeft om uitgebracht te worden in de stabiele release van
Debian: algemeen gesteld geldt dat als er tegen een pakket release-kritieke
bugs gerapporteerd werden, het niet in <q>testing</q> zal geraken en bijgevolg
niet uitgebracht zal worden in <q>stable</q>.</p>

<p>
Onder het totaal aantal <q>testing</q>-bugs worden alle release-kritieke bugs
verstaan die gelden voor in <q>testing</q> beschikbare <tt>pakket/versie</tt>-combinaties voor een release-architectuur.
</p>


<h3><q>Hoe kan het installeren van een pakket in <q>testing</q> mogelijk andere pakketten onklaar maken?</q></h3>

<p>De structuur van de distributiearchieven is dusdanig dat deze slechts één
versie van een pakket kunnen bevatten; een pakket wordt door zijn naam
gedefinieerd. Indien dus het broncodepakket <tt>acmefoo</tt> in <q>testing</q>
geïnstalleerd wordt, samen met zijn binaire pakketten <tt>acme-foo-bin</tt>,
<tt>acme-bar-bin</tt>, <tt>libacme-foo1</tt> en <tt>libacme-foo-dev</tt>,
wordt de oude versie verwijderd.</p>

<p>De oude versie kan echter een binair pakket leveren met een
bibliotheek met oude soname, zoals <tt>libacme-foo0</tt>. Het verwijderen van
het oude <tt>acmefoo</tt> zal het verwijderen van <tt>libacme-foo0</tt> met
zich meebrengen, wat eventuele pakketten die ervan afhankelijk zijn, onklaar
zal maken.</p>

<p>Het is duidelijk dat dit vooral pakketten betreft die een wijzigend geheel
van binaire pakketten aanbieden in een volgende versie (nogmaals,
hoofdzakelijk bibliotheken). Het zal echter ook pakketten betreffen waarover
andere pakketten hebben aangegeven dat ze een specifieke versie ervan
vereisen, in de vorm van ==, &lt;= of &lt;&lt;.</p>

<p>Wanneer het geheel van binaire pakketten dat een broncodepakket levert, op
die manier verandert, zullen alle pakketten die afhankelijk waren van de oude
binaire pakketten, moeten opgewaardeerd worden om de nieuwe binaire pakketten
te gaan vereisen. Omdat de installatie in <q>testing</q> van een dergelijk
broncodepakket alle pakketten in <q>testing</q> onklaar maakt die er
afhankelijk van zijn, moeten er enige voorzorgen getroffen worden: alle
afhankelijke pakketten moeten zelf ook opgewaardeerd worden en klaar zijn
voor installatie, zodat ze niet onklaar gemaakt zullen worden, en eens alles
klaar is, is normaal een handmatige interventie vereist van de releasemanager
of een assistent.</p>

<p>Indien u problemen ervaart met dergelijke gecompliceerde groepen
pakketten, contacteert u best debian-devel of debian-release voor hulp.</p>

<h3><q>Ik versta het nog steeds niet! De <q>testing</q>-scripts zeggen dat
dit pakket een geldige kandidaat is, maar het is nog steeds niet in
<q>testing</q> geraakt.</q></h3>

<p>Dit gebeurt meestal wanneer het installeren van het pakket op een of
andere manier, rechtstreeks of onrechtstreeks, een ander pakket onklaar zou
maken.</p>

<p>Denk eraan om de vereisten van uw pakket na te gaan. Veronderstel dat uw
pakket libtool of libltdl<var>X</var> vereist. Uw pakket zal dan <q>testing</q> niet binnengaan zolang de juiste versie van libtool niet klaar is om er
samen mee naartoe te gaan.</p>

<p>Op zijn beurt zal dit niet gebeuren totdat het installeren van libtool
geen zaken onklaar maakt die reeds aanwezig zijn in <q>testing</q>. Met
andere woorden, totdat alle andere pakketten die libltdl<var>Y</var> vereisen
(waarbij <var>Y</var> de vorige versie is), opnieuw gecompileerd werden en al
hun release-kritieke bugs weggewerkt zijn, enz., zal geen enkel van deze
pakketten in <q>testing</q> belanden.</p>

<p>Dit is waar de <a href="https://release.debian.org/britney/update_output.txt">\
tekstuele uitvoer</a>
[<a href="https://release.debian.org/britney/update_output.txt.gz">met gzip gecomprimeerd</a>]
van pas komt: deze geeft aanwijzingen (zij het zeer beknopte) over welke
pakketten onklaar raken wanneer een geldige kandidaat aan <q>testing</q>
toegevoegd wordt (zie het <a
href="$(DOC)/manuals/developers-reference/pkgs.html#details">\
Referentiehandboek voor ontwikkelaars voor meer details</a>).
</p>

<h3><q>Waarom is het soms moeilijk om pakketten van het type
<kbd>Architecture: all</kbd> in <q>testing</q> te krijgen?</q></h3>

<p>Indien het pakket van het type <kbd>Architecture: all</kbd> geïnstalleerd
moet worden, moet het mogelijk zijn om te voldoen aan al zijn vereisten op
<strong>alle</strong> architecturen. Als het bepaalde pakketten vereist die
enkel gecompileerd kunnen worden op een beperkt aantal architecturen die
Debian ondersteunt, lukt dat niet.</p>

<p>Voorlopig evenwel zal <q>testing</q> geen rekening houden met de
installeerbaarheid van pakketten van het type <kbd>Architecture: all</kbd> op
niet-i386 architecturen. (<q>Het is een grove ingreep en ik ben er niet erg
gelukkig mee dat ik hem gedaan heb, maar hier gaan we dan.</q>
&mdash;aj)</p>

<h3><q>Mijn pakket werd geblokkeerd omdat het verouderd is op sommige architecturen. Wat moet ik doen?</q></h3>

<p>Kijk de toestand van uw pakket na in de
<a href="https://buildd.debian.org/build.php">databank met compilatielogs</a>.
Indien het pakket niet gecompileerd raakt, zal het gemarkeerd staan als
<em>failed</em>; onderzoek de compilatielogs en repareer eventuele problemen
die veroorzaakt worden door de broncode van uw pakket.</p>

<p>Indien u merkt dat de nieuwe versie van uw pakket op sommige architecturen
gecompileerd werd, maar dat het nog niet opduikt in de uitvoer van de
<q>testing</q>-scripts, moet u gewoon een beetje meer geduld hebben tot de
desbetreffende buildd-verantwoordelijke de bestanden naar het Debian archief
uploadt.</p>

<p>Indien u merkt dat de nieuwe versie van uw pakket op sommige
architecturen helemaal niet gecompileerd raakt, ondanks het feit dat u een
reparatie uploadde voor een eerdere mislukking, is de reden wellicht dat het
gemarkeerd staat als aan het wachten op vereisten (Dep-Wait). U kunt ook de
lijst bekijken van deze zogenaamde
<a href="https://buildd.debian.org/stats/">wanna-build states</a> om zich
ervan te vergewissen.</p>

<p>Gewoonlijk raken deze problemen uiteindelijk opgelost, maar indien u
reeds gedurende een lange periode aan het wachten bent (laat ons zeggen twee
weken of langer), verwittig dan de desbetreffende buildd-verantwoordelijke
voor die architectuur, mocht u daarvan een adres kunnen terugvinden op de
<a href="$(HOME)/ports/">webpagina over ports/architecturen</a>, of licht de
mailinglijst voor die architectuur in.</p>

<p>Indien u in het control-bestand een architectuur expliciet weggelaten
heeft uit de lijst 'Architecture', maar dat pakket vroeger wel gecompileerd
werd voor deze architectuur, zult u moeten vragen dat het oude binaire pakket
voor deze architectuur verwijderd wordt uit het archief, vooraleer uw pakket
naar testing kan overgaan. U moet een bugrapport opmaken tegen
<q>ftp.debian.org</q> met de vraag om het pakket voor de architectuur die
niet langer ondersteund wordt, te verwijderen uit het archief van unstable.
Uit beleefdheid moet over het algemeen ook de mailinglijst van de
desbetreffende architectuur/port geïnformeerd worden.</p>

<h3><q>Zijn er uitzonderingen? Ik ben er zeker van dat <tt>acmefoo</tt> zonet
toch in <q>testing</q> binnengekomen is, ondanks het feit dat het niet aan
alle vereisten beantwoordde.</q></h3>

<p>De releasemanagers kunnen de regels op twee manieren omzeilen:</p>

<ul>
  <li>Zij kunnen van oordeel zijn dat het nadeel dat door de installatie van
een nieuwe bibliotheek veroorzaakt zal worden, de zaken eerder beter dan
slechter maakt, en beslissen om het pakket samen met zijn vloot afhankelijke
pakketten binnen te laten.</li>
  <li>Zij kunnen ook pakketten die onklaar zouden gemaakt worden, handmatig
verwijderen uit <q>testing</q>, zodat de nieuwe zaken geïnstalleerd kunnen
worden.</li>
</ul>

<h3><q>Kunt u een reëel, niet-onbelangrijk voorbeeld geven?</q></h3>

<p>Hier volgt er een: wanneer het broncodepakket <tt>apache</tt> in
<q>testing</q> wordt geïnstalleerd, samen met zijn binaire pakketten
<tt>apache</tt>, <tt>apache-common</tt>, <tt>apache-dev</tt> en <tt>apache-doc</tt>,
wordt de oude versie verwijderd.</p>

<p>Alle pakketten met Apache modules vereisen evenwel <code>apache-common (&gt;=
<var>iets</var>), apache-common (&lt;&lt; <var>iets</var>)</code>, en dus
maakt deze wijziging al deze afhankelijke pakketten onklaar. Als gevolg
hiervan moeten alle Apache modules opnieuw gecompileerd worden tegen de
nieuwe versie van Apache, zodat <q>testing</q> geüpdatet kan worden.</p>

<p>Laten we hier iets dieper op ingaan: nadat alle modules in unstable
bijgewerkt werden om te werken met een nieuwe versie van Apache, onderzoeken
de <q>testing</q>-scripts <tt>apache-common</tt> en stellen vast dat dit alle
Apache-modules onklaar maakt, omdat die de volgende vereiste hebben:
<code>Depends: apache-common (&lt;&lt; <var>de huidige versie</var>)</code>.
En daarna onderzoeken zij <tt>libapache-<var>foo</var></tt> en stellen vast
dat het niet geïnstalleerd wordt omdat het de vereiste <code>Depends: apache-common (&gt;= <var>de nieuwe versie</var>)</code> heeft.</p>

<p>Later zullen zij evenwel een andere logica toepassen (soms ingegeven door
een handmatige interventie): zij zullen het feit dat <tt>apache-common</tt>
bepaalde zaken onklaar maakt, negeren en doorgaan met zaken die werken: als
het nog steeds niet werkt nadat we alles gedaan hebben wat in onze
mogelijkheden lag, dan is dat pech, maar misschien <strong>zal</strong> het
toch wel werken. Later zullen ze alle willekeurige <tt>libapache-<var>foo</var></tt>-pakketten onderzoeken en vaststellen dat deze inderdaad werken.</p>

<p>Nadat ze alles geprobeerd hebben, zullen ze nagaan hoeveel pakketten
onklaar gemaakt werden, bepalen of dit beter of slechter is dan wat er
origineel was, en zullen ze ofwel alles aanvaarden of de zaak maar laten
zitten. U ziet dit in <tt>update_output.txt</tt> op de <q><code>recur:</code></q>-regels.</p>

<p>Bijvoorbeeld:</p>

<pre>
         recur: [<var>foo</var> <var>bar</var>] <var>baz</var>
</pre>

<p>In essentie zegt dit: <q>ik heb reeds ontdekt dat <var>foo</var> en
<var>bar</var> de zaken beter maken; nu probeer ik <var>baz</var> om te
zien wat er gebeurt, ook al maakt dit zaken onklaar</q>. De regels van
<tt>update_output.txt</tt> die beginnen met <q><code>accepted</code></q>
geven zaken aan die de dingen blijkbaar beter maken, en de regels met
<q><code>skipped</code></q>, zaken die de dingen slechter maken.</p>

<h3><q>Het bestand <tt>update_output.txt</tt> is totaal onleesbaar!</q></h3>

<p>Dit is geen vraag. ;-)</p>

<p>Laten we een voorbeeld geven:</p>

<pre>
 skipped: cln (0) (150+4)
     got: 167+0: a-40:a-33:h-49:i-45
     * i386: ginac-cint, libginac-dev
</pre>

<p>Dit betekent dat indien <tt>cln</tt> <q>testing</q> binnen gaat, <tt>ginac-cint</tt>
en <tt>libginac-dev</tt> oninstalleerbaar worden in <q>testing</q> op i386.
Merk op dat de architecturen in alfabetische volgorde gecontroleerd worden en
dat enkel de problemen op de eerste architectuur weergegeven worden &mdash;
dat is de reden waarom de architectuur alpha zo vaak voorkomt.</p>

<p>De <q>got</q>-regel bevat het aantal problemen in <q>testing</q> op de
verschillende architecturen (tot aan de eerste architectuur waarvoor een
probleem aangetroffen wordt &mdash; zie hierboven). Het <q>i-45</q> betekent
dat indien <tt>cln</tt> <q>testing</q> zou binnengaan, er 45 oninstalleerbare
pakketten zouden zijn op i386. Sommige van de gegevens boven en onder
<tt>cln</tt> geven aan dat er op dat moment 43 oninstalleerbare pakketten
waren in <q>testing</q> op i386.</p>

<p>De regel <q>skipped: cln (0) (150+4)</q> geeft aan dat er na dit pakket
nog altijd 150 pakketten nagekeken moeten worden vooraleer deze controle van
alle pakketten voltooid zal zijn, en dat reeds 4 pakketten aangetroffen
werden waarvoor geen opwaardering gepland wordt, omdat zij afhankelijke
pakketten onklaar zouden maken. De <q>(0)</q> is irrelevant en u kunt dit
gewoon negeren.</p>

<p>Merk op dat er in één doorloop van een <q>testing</q>-script verschillende
controles worden uitgevoerd op alle pakketten.</p>

<p><em>Oorspronkelijk werden de veel gestelde vragen en antwoorden samengesteld door Jules Bean.</em></p>
# Created: Sat Dec  8 12:44:29 GMT 2001

<h2>Bijkomende informatie</h2>

<p>De volgende pagina's verschaffen bijkomende informatie over de actuele
toestand van testing en over de overgang van pakketten uit unstable naar
testing:</p>

<ul>
<li>Statistieken over binaire pakketten die verouderd zijn voor
<a href="https://release.debian.org/britney/testing_outdate.txt">testing</a>,
<a href="https://release.debian.org/britney/stable_outdate.txt">stable</a>
<li>Vereistenproblemen in
<a href="https://qa.debian.org/debcheck.php?list=INDEX&amp;dist=testing">testing</a>,
<a href="https://qa.debian.org/debcheck.php?list=INDEX&amp;dist=stable">stable</a>
<li>Mooi <a href="https://release.debian.org/migration/">web front-end</a> om
u te helpen uitzoeken waarom pakketten weggehouden worden uit testing
</ul>

<p>Misschien interesseert het u om een oudere
<a href="https://lists.debian.org/debian-devel-0008/msg00906.html">uitleg in
een e-mail</a> te lezen. Het enige belangrijke nadeel ervan is dat hij geen rekening houdt de pakket-pool, omdat deze door James Troup geïmplementeerd werd, nadat de e-mail al geschreven was.</p>

<p>De testing broncode is te vinden op
<a href="https://release.debian.org/britney/update_out_code/">ftp-master</a>.</p>

<p><em>Anthony Towns heeft de implementatie van testing gerealiseerd.</em></p>
